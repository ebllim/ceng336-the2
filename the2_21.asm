;   2036234 İsmail Tüzün
;   2036408 Yunus Emre Zengin
    LIST	P=18F8722
    #INCLUDE    <p18f8722.inc>
    config OSC = HSPLL, FCMEN = OFF, IESO = OFF, PWRT = OFF, BOREN = OFF, WDT = OFF, MCLRE = ON, LPT1OSC = OFF, LVP = OFF, XINST = OFF, DEBUG = ON
;    CONFIG OSC = HSPLL, FCMEN = OFF, IESO = OFF, PWRT = OFF, BOREN = OFF, WDT = OFF, MCLRE = ON, LPT1OSC = OFF, LVP = OFF, XINST = OFF, DEBUG = ON

Nfruit   equ 0x21
Nfruit1   equ 0x22
Nfruit10   equ 0x23

lives	equ 0x24
L   equ 0x25
L1  equ 0x26
L2  equ 0x27
Time	equ 0x28
Time1	equ 0x29
Time10	equ 0x2A

w_temp	equ 0x2B
status_temp equ 0x2C
pclath_temp equ 0x2D

stateT	equ 0x2E    ; 0: time - 1:move - 2:blink
counter0    equ 0x2F
portb_var   equ 0x30
portb_P2    equ 0x31
direction_P2	equ 0x32 ; 0th:enable - 4th:right - 5th:left - 6th:up - 7th:down

coordinate_x    equ 0x33
coordinate_y    equ 0x34
stateBlink      equ 0x35

moveUnitS   equ 0x36
blinkUnitS   equ 0x37

moveUnit   equ 0x38
blinkUnit   equ 0x39

fruit1  equ 0x3A
fruit2  equ 0x3B
fruit3  equ 0x3C
fruit4  equ 0x3D

fruitParameter  equ 0x3E
fruitParameter2 equ 0x3F

seed    equ 0x40

fruitUnit1  equ 0x41
fruitUnit2  equ 0x42
fruitUnit3  equ 0x43
fruitUnit4  equ 0x44
fruitUnitS  equ 0x45
fruitCreateUnit equ 0x46
fruitCreateUnitS     equ 0x47

snakeX   equ 0x48
snakeY   equ 0x49
tmp	equ 0x4A

score   equ 0x4B
score1  equ 0x4C
score10 equ 0x4D

fmax    equ 0x4E
unitState   equ 0x4F

    org	0x00
    goto	init

    org	0x08
    goto isrH

    org	0x18
    goto isrL

    org 0x50
table:
    rlncf   WREG,W          ; multiply index X2
    addwf   PCL	; modify program counter
    retlw   b'00111111'	; 7-Segment = 0
    retlw   b'00000110'	; 7-Segment = 1
    retlw   b'01011011'	; 7-Segment = 2
    retlw   b'01001111'	; 7-Segment = 3
    retlw   b'01100110'	; 7-Segment = 4
    retlw   b'01101101'	; 7-Segment = 5
    retlw   b'01111101'	; 7-Segment = 6
    retlw   b'00000111'	; 7-Segment = 7
    retlw   b'01111111'	; 7-Segment = 8
    retlw   b'01100111'	; 7-Segment = 9


lifeTable:
    rlncf   WREG,W          ; multiply index X2
    addwf   PCL	; modify program counter
    retlw   b'00000000'
    retlw   b'00000001'
    retlw   b'00000011'
    retlw   b'00000111'
    retlw   b'00001111'
    retlw   b'00011111'
    retlw   b'00111111'



point_blink:
    movff   blinkUnitS, blinkUnit

    incf    coordinate_x
    incf    coordinate_y

    dcfsnz  coordinate_x
    goto    x0_sec_blink
    dcfsnz  coordinate_x
    goto    x1_sec_blink
    dcfsnz  coordinate_x
    goto    x2_sec_blink
    dcfsnz  coordinate_x
    goto    x3_sec_blink

x0_sec_blink
    dcfsnz  coordinate_y
    btg     LATC,0
    dcfsnz  coordinate_y
    btg     LATC,1
    dcfsnz  coordinate_y
    btg     LATC,2
    dcfsnz  coordinate_y
    btg     LATC,3
    return
x1_sec_blink
    dcfsnz  coordinate_y
    btg     LATD,0
    dcfsnz  coordinate_y
    btg     LATD,1
    dcfsnz  coordinate_y
    btg     LATD,2
    dcfsnz  coordinate_y
    btg     LATD,3
    return
x2_sec_blink
    dcfsnz  coordinate_y
    btg     LATE,0
    dcfsnz  coordinate_y
    btg     LATE,1
    dcfsnz  coordinate_y
    btg     LATE,2
    dcfsnz  coordinate_y
    btg     LATE,3
    return
x3_sec_blink
    dcfsnz  coordinate_y
    btg     LATF,0
    dcfsnz  coordinate_y
    btg     LATF,1
    dcfsnz  coordinate_y
    btg     LATF,2
    dcfsnz  coordinate_y
    btg     LATF,3
    return


init:
    ; Disable interupts
    clrf    INTCON
    clrf    INTCON2



    setf    ADCON1
    movlw   b'00010000'
    movwf   TRISA
    movlw   b'00111111'
    movwf   TRISC

    ; set initial values
    movlw   d'15'
    movwf   Nfruit
    movlw   d'1'
    movwf   Nfruit10
    movlw   d'5'
    movwf   Nfruit1
    movlw   d'3'
    movwf   lives
    movwf   L

    clrf    unitState

    clrf    PORTA
    clrf    PORTB

    clrf    PORTC

    clrf    PORTE

    clrf    PORTD
    clrf    PORTJ
    clrf    TRISJ
    clrf    PORTH
    clrf    TRISH


    ;Enable interupts before main
main:
    ;



PHASE1:

    call   lightenD3P1
    call   lightenD2P1
    call   lightenD1P1
    call   lightenD0P1


    btfsc   PORTA, 4
    goto    DebounceRA4
    btfsc   PORTC, 0
    goto    DebounceRC0
    btfsc   PORTC, 1
    goto    DebounceRC1
    btfsc   PORTC, 2
    goto    DebounceRC2
    btfsc   PORTC, 3
    goto    DebounceRC3
    btfsc   PORTC, 4
    goto    DebounceRC4
    btfsc   PORTC, 5
    goto    DebounceRC5
    goto    PHASE1

DebounceRA4:
    call   lightenD3P1
    call   lightenD2P1
    call   lightenD1P1
    call   lightenD0P1
    btfsc   PORTA, 4
    goto    DebounceRA4
    goto    PHASE2init

DebounceRC0:
    call   lightenD3P1
    call   lightenD2P1
    call   lightenD1P1
    call   lightenD0P1
    btfsc   PORTC, 0
    goto    DebounceRC0
    call    decrementNfruit
    goto    PHASE1
DebounceRC1:
    call   lightenD3P1
    call   lightenD2P1
    call   lightenD1P1
    call   lightenD0P1
    btfsc   PORTC, 1
    goto    DebounceRC1
    call    incrementNfruit
    goto    PHASE1
DebounceRC2:
    call   lightenD3P1
    call   lightenD2P1
    call   lightenD1P1
    call   lightenD0P1
    btfsc   PORTC, 2
    goto    DebounceRC2
    call    decrementLife
    goto    PHASE1
DebounceRC3:
    call   lightenD3P1
    call   lightenD2P1
    call   lightenD1P1
    call   lightenD0P1
    btfsc   PORTC, 3
    goto    DebounceRC3
    call    incrementLife
    goto    PHASE1
DebounceRC4:
    call   lightenD3P1
    call   lightenD2P1
    call   lightenD1P1
    call   lightenD0P1
    btfsc   PORTC, 4
    goto    DebounceRC4
    call    decrementL
    goto    PHASE1
DebounceRC5:
    call   lightenD3P1
    call   lightenD2P1
    call   lightenD1P1
    call   lightenD0P1
    btfsc   PORTC, 5
    goto    DebounceRC5
    call    incrementL
    goto    PHASE1

PHASE2init:
    clrf    direction_P2
    clrf    TRISA
    movf    PORTB, w
    movlw   d'0'
    movwf   Time
    movwf   Time1
    movwf   Time10
    call    lifeSet

    movf    L, w

    movff   Nfruit, fmax

    ;Configure Output Ports
    clrf    TRISA
    clrf    LATA
    clrf    TRISC
    clrf    LATC
    clrf    TRISD
    clrf    LATD
    clrf    TRISE
    clrf    LATE
    clrf    TRISF
    clrf    LATF

    clrf    score
    clrf    score1
    clrf    score10

    clrf    snakeX
    clrf    snakeY

    clrf    coordinate_x
    clrf    coordinate_y

    clrf    fruitUnit1
    clrf    fruitUnit2
    clrf    fruitUnit3
    clrf    fruitUnit4
    bsf     fruitUnit1, 7
    bsf     fruitUnit2, 7
    bsf     fruitUnit3, 7
    bsf     fruitUnit4, 7


    call    Lset
    movlw   0xAD
    movwf   seed

    ;Disable interrupts
    clrf    INTCON
    clrf    INTCON2

    ;Configure Input/Interrupt Ports
    movlw   b'11110000'
    movwf   TRISB
    bsf     INTCON2, 7  ;External Pull-ups are enabled, Internal Pull-ups are disabled
    clrf    PORTB

    ;Initialize Timer0
    movlw   b'01000111' ;Disable Timer0 by setting TMR0ON to 0 (for now)
                        ;Configure Timer0 as an 8-bit timer/counter by setting T08BIT to 1
                        ;Timer0 increment from internal clock with a prescaler of 1:256.
    movwf   T0CON

    ;Enable interrupts
    movlw   b'11101000' ;Enable Global, peripheral, Timer0 and RB interrupts by setting GIE, PEIE, TMR0IE and RBIE bits to 1
    movwf   INTCON

    bsf	    RCON, 7
    bsf	    INTCON2, TMR0IP


    bsf     T0CON, 7    ;Enable Timer0 by setting TMR0ON to 1
PHASE2:
    call    gameOver
    btfsc   WREG, 0
    goto    PHASE3

    call    lightenD3P2
    call    lightenD2P2
    call    lightenD1P2
    call    lightenD0P2

    call    lifeSet

    btfsc   stateT, 5         ; Should we increment elapsed time?
    call    incrementTime


    call    fruitCheck

    clrf    WREG
    cpfsgt  fruitCreateUnit
    call    fruitCreate
   

    movff   snakeX, coordinate_x
    movff   snakeY, coordinate_y

    clrf    WREG
    cpfsgt  blinkUnit
    call    point_blink	    ; blinkin basinda biseyler var onu untuma

    
    clrf    WREG
    cpfsgt  moveUnit
    call    move_snake

    call    collision_check


 
    goto PHASE2

PHASE3:

    clrf    TRISA
    clrf    LATA
    bsf     TRISA, 4

    clrf    TRISC
    clrf    LATC
    clrf    TRISD
    clrf    LATD
    clrf    TRISE
    clrf    LATE
    clrf    TRISF
    clrf    LATF

     ;Disable interrupts
    clrf    INTCON
    clrf    INTCON2

PHASE3_loop
    call    lightenD3P3
    call    lightenD2P3
    call    lightenD1P3
    call    lightenD0P3

    btfsc   PORTA, 4
    goto    DebounceRA4P3
    goto PHASE3_loop

DebounceRA4P3:
    call    lightenD3P3
    call    lightenD2P3
    call    lightenD1P3
    call    lightenD0P3
    btfsc   PORTA, 4
    goto    DebounceRA4P3
    goto    init



isrH:
    call    save_registers  ;Save current content of STATUS and PCLATH registers to be able to restore them later
;;;;;;;;;;;;;;;;;;;;;;;; Timer interrupt handler part ;;;;;;;;;;;;;;;;;;;;;;;;;;
timer_interrupt:
    incf	counter0, f              ;Timer interrupt handler part begins here by incrementing count variable
    movf	counter0, w              ;Move count to Working register
    sublw	d'101'                  ;Decrement 100 from Working register
    btfss	STATUS, Z               ;Is the result Zero?
    goto	timer_interrupt_exit    ;No, then exit from interrupt service routine
    clrf	counter0                 ;Yes, then clear count variable
    incf 	stateT, f                ;Complement our state variable which controls on/off state of LED0
    incf    stateBlink, f
    decf    moveUnit, f
    decf    blinkUnit, f
    decf    fruitCreateUnit

    incf    unitState
    btfss   unitState, 0
    goto	timer_interrupt_exit

    btfsc   fruitUnit1, 7
    goto    fr2dec

    tstfsz  fruitUnit1
    decf    fruitUnit1
fr2dec

    btfsc   fruitUnit2, 7
    goto    fr3dec

    tstfsz  fruitUnit2
    decf    fruitUnit2
fr3dec

    btfsc   fruitUnit3, 7
    goto    fr4dec

    tstfsz  fruitUnit3
    decf    fruitUnit3

fr4dec
    btfsc   fruitUnit4, 7
    goto    timer_interrupt_exit

    tstfsz  fruitUnit4
    decf    fruitUnit4


timer_interrupt_exit:
    bcf	    INTCON, 2		    ;Clear TMROIF
    movlw	d'244'               ;256-61=195; 195*256*100 = 249600 instruction cycle;
    movwf	TMR0
    call	restore_registers   ;Restore STATUS and PCLATH registers to their state before interrupt occurs
    retfie

;;;;;;;;;;;;;;;;;;; PORTB on change interrupt handler part ;;;;;;;;;;;;;;;;;;;;;
isrL:
    call    save_registers  ;Save current content of STATUS and PCLATH registers to be able to restore them later
rb_interrupt

    movf	PORTB, w
    movwf	portb_var
    clrf	WREG
    cpfseq	portb_var
    goto	pressedISRL
    goto	releasedISRL


pressedISRL
    movff	portb_var, direction_P2
    goto	rb_interupt_exit
releasedISRL
    bsf		direction_P2, 0
    goto	rb_interupt_exit

rb_interupt_exit
    bcf		INTCON, 0           ;Clear PORTB on change FLAG
    call	restore_registers   ;Restore STATUS and PCLATH registers to their state before interrupt occurs
    retfie


;;;;;;;;;;;; Register handling for proper operation of main program ;;;;;;;;;;;;
save_registers:
    movwf 	w_temp          ;Copy W to TEMP register
    swapf 	STATUS, w       ;Swap status to be saved into W
    clrf 	STATUS          ;bank 0, regardless of current bank, Clears IRP,RP1,RP0
    movwf 	status_temp     ;Save status to bank zero STATUS_TEMP register
    movf 	PCLATH, w       ;Only required if using pages 1, 2 and/or 3
    movwf 	pclath_temp     ;Save PCLATH into W
    clrf 	PCLATH          ;Page zero, regardless of current page
    return

restore_registers:
    movf 	pclath_temp, w  ;Restore PCLATH
    movwf 	PCLATH          ;Move W into PCLATH
    swapf 	status_temp, w  ;Swap STATUS_TEMP register into W
    movwf 	STATUS          ;Move W into STATUS register
    swapf 	w_temp, f       ;Swap W_TEMP
    swapf 	w_temp, w       ;Swap W_TEMP into W
    return

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DELAY:				; Time Delay Routines
    movlw d'50'			; Copy 50 to W
    movwf L2                    ; Copy W into L2

LOOP2:
    movlw d'255'                   ; Copy 255 into W
    movwf L1			; Copy W into L1

LOOP1:
    decfsz L1,F			    ; Decrement L1. If 0 Skip next instruction
        goto LOOP1		    ; ELSE Keep counting down
    decfsz L2,F			    ; Decrement L2. If 0 Skip next instruction
        goto LOOP2		    ; ELSE Keep counting down
    return

;;	PHASE1 Fuctions	    ;;
incrementL:
    incf    L, 1
    movlw   d'3'
    cpfslt  L
    movwf   L
    return
decrementL:
    decf    L, 1
    btfsc   L, 7
    clrf    L
    return
incrementLife:
    incf    lives, 1
    movlw   d'6'
    cpfslt  lives
    movwf   lives
    return
decrementLife:
    decf    lives, 1
    btfsc   lives, 7
    clrf    lives
    return
incrementNfruit:
    movlw   d'99'
    cpfseq  Nfruit
    goto    infactive
    return
infactive
    incf    Nfruit, 1
    movlw   d'9'
    cpfseq  Nfruit1
    goto    inficr1
    clrf    Nfruit1
    incf    Nfruit10, 1
    return
inficr1
    incf    Nfruit1, 1
    return
decrementNfruit:
    movlw   d'0'
    cpfseq  Nfruit
    goto    dnfactive
    return
dnfactive
    decf    Nfruit, 1
    movlw   d'0'
    cpfseq  Nfruit1
    goto    dnficr1
    movlw   d'9'
    movwf   Nfruit1
    decf    Nfruit10, 1
    return
dnficr1
    decf    Nfruit1, 1
    return
lightenD3P1:
    movf    L, w
    call    table
    movwf   LATJ
    bsf     LATH, 0
    call    DELAY
    bcf     LATH, 0
    return
lightenD2P1:
    movf    lives, w
    call    table
    movwf   LATJ
    bsf     LATH, 1
    call    DELAY
    bcf     LATH, 1
    return
lightenD1P1:
    movf    Nfruit10, w
    call    table
    movwf   LATJ
    bsf     LATH, 2
    call    DELAY
    bcf     LATH, 2
    return
lightenD0P1:
    movf    Nfruit1, w
    call    table
    movwf   LATJ
    bsf     LATH, 3
    call    DELAY
    bcf     LATH, 3
    return

;;	PHASE2 Functions	;;


lifeSet:
    movf    lives, w
    call    lifeTable
    movwf   LATA
    return
incrementTime:
    clrf    stateT
    movlw   d'99'
    cpfseq  Time
    goto    iTimeactive
    goto    iTimereset
iTimeactive
    incf    Time, 1
    movlw   d'9'
    cpfseq  Time1
    goto    iTimeicr1
    clrf    Time1
    incf    Time10, 1
    return
iTimeicr1
    incf    Time1, 1
    return
iTimereset
    clrf    Time
    clrf    Time1
    clrf    Time10
    return


lightenD3P2:
    movf    Nfruit10, w
    call    table
    movwf   LATJ
    bsf     LATH, 0
    call    DELAY
    bcf     LATH, 0
    return
lightenD2P2:
    movf    Nfruit1, w
    call    table
    movwf   LATJ
    bsf     LATH, 1
    call    DELAY
    bcf     LATH, 1
    return
lightenD1P2:
    movf    Time10, w
    call    table
    movwf   LATJ
    bsf     LATH, 2
    call    DELAY
    bcf     LATH, 2
    return
lightenD0P2:
    movf    Time1, w
    call    table
    movwf   LATJ
    bsf     LATH, 3
    call    DELAY
    bcf     LATH, 3
    return


move_snake:

;    btfss   direction_P2,0
;    return
;    bcf     direction_P2,0

    movff   moveUnitS, moveUnit
    btfsc   direction_P2,4 ; right
    goto    go_right
    btfsc   direction_P2,5 ; left
    goto    go_left
    btfsc   direction_P2,6 ; up
    goto    go_up
    btfsc   direction_P2,7 ; down
    goto    go_down

go_right
    movlw   d'3'
    cpfslt  snakeX
    goto    die
    incf    snakeX, f
    return
go_left
    movlw   d'0'
    cpfsgt  snakeX
    goto    die
    decf    snakeX, f
    return
go_up
    movlw   d'0'
    cpfsgt  snakeY
    goto    die
    decf    snakeY, f
    return
go_down
    movlw   d'3'
    cpfslt  snakeY
    goto    die
    incf    snakeY, f
    return

die
    call    decrementLife; geberecen geberecen
    return

Lset:
    movlw   d'0'
    cpfsgt  L
    goto Lset0
    movlw   d'1'
    cpfsgt  L
    goto Lset1
    movlw   d'2'
    cpfsgt  L
    goto Lset2
    movlw   d'3'
    cpfsgt  L
    goto Lset3

Lset0
    movlw   d'1'
    goto LsetEnd
Lset1
    movlw   d'2'
    goto LsetEnd
Lset2
    movlw   d'4'
    goto LsetEnd
Lset3
    movlw   d'8'
    goto LsetEnd

LsetEnd
    movwf   blinkUnit
    movwf   blinkUnitS
    rlncf   WREG, W
    rlncf   WREG, W
    movwf   moveUnit
    movwf   moveUnitS
    rlncf   WREG, W
    movwf   fruitCreateUnit
    movwf   fruitCreateUnitS
    rlncf   WREG, W
;    rlncf   WREG, W
    movwf   fruitUnitS

    movlw   d'3'
    cpfsgt  L
    decf    fruitUnitS ; bu sadece 3. level i\E7in olacak

    return


parseFruit: ; 1 => x - 2 => y - 3 => time
    ; set wreg as fruit before call
    dcfsnz  fruitParameter
    goto    ret_x
    dcfsnz  fruitParameter
    goto    ret_y
    dcfsnz  fruitParameter
    goto    ret_time


ret_x
    movwf   fruitParameter
    movlw   b'00000011'
    andwf   fruitParameter, w
    movwf   coordinate_x
    return
ret_y
    movwf   fruitParameter
    movlw   b'11000000'
    andwf   fruitParameter, w
    rrncf   WREG, w
    rrncf   WREG, w
    rrncf   WREG, w
    rrncf   WREG, w
    rrncf   WREG, w
    rrncf   WREG, w
    movwf   coordinate_y
    return
ret_time
    movwf   fruitParameter
    movlw   b'00111100'
    andwf   fruitParameter, w
    rrncf   WREG, w
    rrncf   WREG, w
    return

setFruit: ; 1 => x - 2 => y - 3 => time
    ; set wreg as fruit before call
    ; set value to fruitParameter2
    dcfsnz  fruitParameter
    goto    set_x
    dcfsnz  fruitParameter
    goto    set_y
    dcfsnz  fruitParameter
    goto    set_time


set_x
    iorwf   fruitParameter2, w
    return
set_y
    rlncf   fruitParameter2
    rlncf   fruitParameter2
    rlncf   fruitParameter2
    rlncf   fruitParameter2
    rlncf   fruitParameter2
    rlncf   fruitParameter2
    iorwf   fruitParameter2, w
    return
set_time
    rlncf   fruitParameter2
    rlncf   fruitParameter2
    iorwf   fruitParameter2, w
    return

calc_seed:
    btfss   seed, 7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    goto    seedCarryNotExist
    rlncf   seed, f
    movlw   0xB8
    xorwf   seed, f
    return
seedCarryNotExist
    rlncf   seed, f
    return

fruitCreate:
    tstfsz  fmax
    goto    do_fruitCreate
    return
do_fruitCreate
    decf    fmax
    call    calc_seed
    ;;;;;;;;;;;; fruit1 ;;;;;;;;;;;;
    btfss   fruitUnit1,7
    goto    fruit_create_2

    movff   seed, fruit1
    movlw   b'11000011'
    andwf   fruit1, f

    movlw   d'1'
    movwf   fruitParameter
    movf    fruit1, w
    call    parseFruit
    movlw   d'2'
    movwf   fruitParameter
    movf    fruit1, w
    call    parseFruit
    call    point_on

;    setf    fruitUnit1
;    bcf     fruitUnit1, 7
    movff   fruitUnitS, fruitUnit1
    bcf     fruitUnit1, 7
    goto    exit_fruit_create
    ;;;;;;;;;;;; fruit1 ;;;;;;;;;;;;

fruit_create_2
    ;;;;;;;;;;;; fruit2 ;;;;;;;;;;;;
    btfss   fruitUnit2,7
    goto    fruit_create_3

    movff   seed, fruit2
    movlw   b'11000011'
    andwf   fruit2, f

    movlw   d'1'
    movwf   fruitParameter
    movf    fruit2, w
    call    parseFruit
    movlw   d'2'
    movwf   fruitParameter
    movf    fruit2, w
    call    parseFruit
    call    point_on
    movff   fruitUnitS, fruitUnit2
    bcf     fruitUnit2, 7

    goto    exit_fruit_create
    ;;;;;;;;;;;; fruit2 ;;;;;;;;;;;;

fruit_create_3
    ;;;;;;;;;;;; fruit3 ;;;;;;;;;;;;
    btfss   fruitUnit3,7
    goto    fruit_create_4

    movff   seed, fruit3
    movlw   b'11000011'
    andwf   fruit3, f

    movlw   d'1'
    movwf   fruitParameter
    movf    fruit3, w
    call    parseFruit
    movlw   d'2'
    movwf   fruitParameter
    movf    fruit3, w
    call    parseFruit
    call    point_on
    movff   fruitUnitS, fruitUnit3
    bcf     fruitUnit3, 7

    goto    exit_fruit_create
    ;;;;;;;;;;;; fruit3 ;;;;;;;;;;;;

fruit_create_4
    ;;;;;;;;;;;; fruit4 ;;;;;;;;;;;;
    btfss   fruitUnit4,7
    goto    exit_fruit_create

    movff   seed, fruit4
    movlw   b'11000011'
    andwf   fruit4, f

    movlw   d'1'
    movwf   fruitParameter
    movf    fruit4, w
    call    parseFruit
    movlw   d'2'
    movwf   fruitParameter
    movf    fruit4, w
    call    parseFruit
    call    point_on

    movff   fruitUnitS, fruitUnit4
    bcf     fruitUnit4, 7

;    goto    exit_fruit_create
    ;;;;;;;;;;;; fruit4 ;;;;;;;;;;;;



exit_fruit_create

    movff   fruitCreateUnitS, fruitCreateUnit
    return

fruitCheck:

    ;;;;;;;;;;;;;;;fruit1;;;;;;;;;;;;
fruit_check_1
    tstfsz  fruitUnit1
    goto    fruit_check_2


    movlw   d'1'
    movwf   fruitParameter
    movf    fruit1, w
    call    parseFruit

    movlw   d'2'
    movwf   fruitParameter
    movf    fruit1, w
    call    parseFruit

    call    point_off
    call    decrementNfruit
    bsf     fruitUnit1, 7
    ;;;;;;;;;;;;;;;fruit1;;;;;;;;;;;;


    ;;;;;;;;;;;;;;;fruit2;;;;;;;;;;;;
fruit_check_2
    tstfsz   fruitUnit2
    goto    fruit_check_3


    movlw   d'1'
    movwf   fruitParameter
    movf    fruit2, w
    call    parseFruit

    movlw   d'2'
    movwf   fruitParameter
    movf    fruit2, w
    call    parseFruit

    call    point_off
    call    decrementNfruit
    bsf     fruitUnit2, 7
    ;;;;;;;;;;;;;;;fruit2;;;;;;;;;;;;


    ;;;;;;;;;;;;;;;fruit3;;;;;;;;;;;;
fruit_check_3
    tstfsz   fruitUnit3
    goto    fruit_check_4


    movlw   d'1'
    movwf   fruitParameter
    movf    fruit3, w
    call    parseFruit

    movlw   d'2'
    movwf   fruitParameter
    movf    fruit3, w
    call    parseFruit

    call    point_off
    call    decrementNfruit
    bsf     fruitUnit3, 7
    ;;;;;;;;;;;;;;;fruit3;;;;;;;;;;;;

    ;;;;;;;;;;;;;;;fruit4;;;;;;;;;;;;
fruit_check_4
    tstfsz   fruitUnit4
    return


    movlw   d'1'
    movwf   fruitParameter
    movf    fruit4, w
    call    parseFruit

    movlw   d'2'
    movwf   fruitParameter
    movf    fruit4, w
    call    parseFruit

    call    point_off
    call    decrementNfruit
    bsf     fruitUnit4, 7
    ;;;;;;;;;;;;;;;fruit4;;;;;;;;;;;;

    return


collision_check:
    movf    snakeX, w
    rlncf   WREG
    rlncf   WREG
    addwf   snakeY, w
    rrncf   WREG
    rrncf   WREG


    movwf   tmp
    btfsc   fruitUnit1, 7
    goto    ceat2
    xorwf   fruit1, w
    btfsc   STATUS, Z
    goto    eat1

ceat2
    btfsc   fruitUnit2, 7
    goto    ceat3
    movf    tmp, w
    xorwf   fruit2, w
    btfsc   STATUS, Z
    goto    eat2

ceat3
    btfsc   fruitUnit3, 7
    goto    ceat4
    movf    tmp, w
    xorwf   fruit3, w
    btfsc   STATUS, Z
    goto    eat3

ceat4
    btfsc   fruitUnit4, 7
    return
    movf    tmp, w
    xorwf   fruit4, w
    btfsc   STATUS, Z
    goto    eat4
    return

eat1
    clrf    fruitUnit1
    bsf	    fruitUnit1, 7
    call    decrementNfruit
    movff   snakeX, coordinate_x
    movff   snakeY, coordinate_y
    call    point_off
    call    incrementScore
    return
eat2
    clrf    fruitUnit2
    bsf	    fruitUnit2, 7
    call    decrementNfruit
    movff   snakeX, coordinate_x
    movff   snakeY, coordinate_y
    call    point_off
    call    incrementScore
    return
eat3
    clrf    fruitUnit3
    bsf	    fruitUnit3, 7
    call    decrementNfruit
    movff   snakeX, coordinate_x
    movff   snakeY, coordinate_y
    call    point_off
    call    incrementScore
    return
eat4
    clrf    fruitUnit4
    bsf	    fruitUnit4, 7
    call    decrementNfruit
    movff   snakeX, coordinate_x
    movff   snakeY, coordinate_y
    call    point_off
    call    incrementScore
    return

point_on:
    incf    coordinate_x
    incf    coordinate_y

    dcfsnz  coordinate_x
    goto    x0_sec_on
    dcfsnz  coordinate_x
    goto    x1_sec_on
    dcfsnz  coordinate_x
    goto    x2_sec_on
    dcfsnz  coordinate_x
    goto    x3_sec_on

x0_sec_on
    dcfsnz  coordinate_y
    bsf     LATC,0
    dcfsnz  coordinate_y
    bsf     LATC,1
    dcfsnz  coordinate_y
    bsf     LATC,2
    dcfsnz  coordinate_y
    bsf     LATC,3
    return
x1_sec_on
    dcfsnz  coordinate_y
    bsf     LATD,0
    dcfsnz  coordinate_y
    bsf     LATD,1
    dcfsnz  coordinate_y
    bsf     LATD,2
    dcfsnz  coordinate_y
    bsf     LATD,3
    return
x2_sec_on
    dcfsnz  coordinate_y
    bsf     LATE,0
    dcfsnz  coordinate_y
    bsf     LATE,1
    dcfsnz  coordinate_y
    bsf     LATE,2
    dcfsnz  coordinate_y
    bsf     LATE,3
    return
x3_sec_on
    dcfsnz  coordinate_y
    bsf     LATF,0
    dcfsnz  coordinate_y
    bsf     LATF,1
    dcfsnz  coordinate_y
    bsf     LATF,2
    dcfsnz  coordinate_y
    bsf     LATF,3
    return

point_off:
    incf    coordinate_x
    incf    coordinate_y

    dcfsnz  coordinate_x
    goto    x0_sec_off
    dcfsnz  coordinate_x
    goto    x1_sec_off
    dcfsnz  coordinate_x
    goto    x2_sec_off
    dcfsnz  coordinate_x
    goto    x3_sec_off

x0_sec_off
    dcfsnz  coordinate_y
    bcf     LATC,0
    dcfsnz  coordinate_y
    bcf     LATC,1
    dcfsnz  coordinate_y
    bcf     LATC,2
    dcfsnz  coordinate_y
    bcf     LATC,3
    return
x1_sec_off
    dcfsnz  coordinate_y
    bcf     LATD,0
    dcfsnz  coordinate_y
    bcf     LATD,1
    dcfsnz  coordinate_y
    bcf     LATD,2
    dcfsnz  coordinate_y
    bcf     LATD,3
    return
x2_sec_off
    dcfsnz  coordinate_y
    bcf     LATE,0
    dcfsnz  coordinate_y
    bcf     LATE,1
    dcfsnz  coordinate_y
    bcf     LATE,2
    dcfsnz  coordinate_y
    bcf     LATE,3
    return
x3_sec_off
    dcfsnz  coordinate_y
    bcf     LATF,0
    dcfsnz  coordinate_y
    bcf     LATF,1
    dcfsnz  coordinate_y
    bcf     LATF,2
    dcfsnz  coordinate_y
    bcf     LATF,3
    return

gameOver:
    clrf    WREG
    tstfsz  Nfruit
    goto    nextCheck
    setf    WREG
    return
nextCheck
    tstfsz  lives
    return
    setf    WREG
    return

incrementScore:
    movlw   d'99'
    cpfseq  score
    goto    infactiveScore
    return
infactiveScore
    incf    score, 1
    movlw   d'9'
    cpfseq  score1
    goto    inficr1Score
    clrf    score1
    incf    score10, 1
    return
inficr1Score
    incf    score1, 1
    return


lightenD3P3:
    tstfsz  lives
    goto    at
    movlw   b'00111110'
    goto    nat
at
    movlw   b'01101101'
nat
    movwf   LATJ
    bsf     LATH, 0
    call    DELAY
    bcf     LATH, 0
    return
lightenD2P3:
    movf    lives, w
    call    table
    movwf   LATJ
    bsf     LATH, 1
    call    DELAY
    bcf     LATH, 1
    return

lightenD1P3:
    movf    score10, w
    call    table
    movwf   LATJ
    bsf     LATH, 2
    call    DELAY
    bcf     LATH, 2
    return
lightenD0P3:
    movf    score1, w
    call    table
    movwf   LATJ
    bsf     LATH, 3
    call    DELAY
    bcf     LATH, 3
    return

    end

